package fr.univlille.iutinfo.m3105.q1;

import javafx.application.Platform;
import javafx.scene.control.Label;

public class Chronometre implements Observer {
	
	private int secondes;
	private Label l;
	
	public Chronometre() {
		this(null, -1);
	}
	
	public Chronometre(Label l) {
		this(l, -1);
	}
	
	public Chronometre(int past) {
		this(null, past);
	}
	
	public Chronometre(Label l, int past) {
		this.secondes = past;
		this.l = l;
	}

	@Override
	public void update(Subject subj) {
		this.secondes++;
		Platform.runLater( () -> { 
			l.setText(this.secondes + "");
		});
	}

	@Override
	public void update(Subject subj, Object data) {
		
	}
	
	public int getTime() {
		return this.secondes;
	}
	
}
