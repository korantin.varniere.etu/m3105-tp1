package fr.univlille.iutinfo.m3105.q1;

public class MainQ1 {

	public static void main(String[] args) {
		Timer timer = new Timer();
		Chronometre chrono = new Chronometre();
		
		timer.attach(chrono);
		timer.start();
	}
	
}
