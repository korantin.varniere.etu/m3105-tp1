package fr.univlille.iutinfo.m3105.q1;


public class Timer extends Subject {
	
	private final TimerThread THREAD;
	
	public Timer() {
		this.THREAD = new TimerThread(this);
	}

	public void start() {
		this.THREAD.start();
	}

	public void stopRunning() {
		this.THREAD.interrupt();
	}

}
