package fr.univlille.iutinfo.m3105.q1;

import java.util.HashSet;
import java.util.Set;

public abstract class Subject {

	private Set<Observer> list = new HashSet<Observer>();
	
	protected void notifyObservers() {
		for (Observer observer : list) {
			observer.update(this);
		}
	}

	protected void notifyObservers(Object data) {
		for (Observer observer : list) {
			observer.update(this, data);
		}
	}

	public void attach(Observer observer) {
		list.add(observer);
	}

	public void detach(Observer observer) {
		list.remove(observer);
	}
}
