package fr.univlille.iutinfo.m3105.q1;

public class TimerThread extends Thread {
	
	private final Timer TIMER;
	
	public TimerThread(Timer timer) {
		this.TIMER = timer;
	}
	
	public void run(){
		while (true) {
			try {
				this.TIMER.notifyObservers();
				sleep(1000);
			} catch (InterruptedException e) {}
		}
	}
	
}
