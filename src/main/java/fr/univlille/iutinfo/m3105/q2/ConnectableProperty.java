package fr.univlille.iutinfo.m3105.q2;

import fr.univlille.iutinfo.m3105.q1.Observer;
import fr.univlille.iutinfo.m3105.q1.Subject;

public class ConnectableProperty extends ObservableProperty implements Observer {

	public void connectTo(ConnectableProperty p2) {
		p2.attach(this);
		this.update(p2, p2.getValue());
	}

	public void biconnectTo(ConnectableProperty p2) {
		p2.attach(this);
		this.attach(p2);
	}

	public void unconnectFrom(ConnectableProperty p2) {
		p2.detach(this);
	}

	@Override
	public void update(Subject subj) {
	}

	@Override
	public void update(Subject subj, Object data) {
		if (this.getValue() != (data)) {
			this.setValue(data);
		}
	}

}
