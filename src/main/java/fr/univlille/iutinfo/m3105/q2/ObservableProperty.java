package fr.univlille.iutinfo.m3105.q2;

import fr.univlille.iutinfo.m3105.q1.Subject;

public class ObservableProperty extends Subject {
	
	protected Object obj;
	
	public void setValue(Object i) {
		obj = i;
		notifyObservers(obj);
	}

	public Object getValue() {
		return obj;
	}

	/*public void attach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}

	public void detach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}*/

}
