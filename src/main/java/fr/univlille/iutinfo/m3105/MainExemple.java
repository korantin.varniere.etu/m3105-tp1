package fr.univlille.iutinfo.m3105;

import fr.univlille.iutinfo.m3105.q1.Chronometre;
import fr.univlille.iutinfo.m3105.q1.Timer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class MainExemple extends Application {

    @Override
    public void start(Stage stage) {
    	Label l = new Label();
    	
    	Timer timer = new Timer();
		Chronometre chrono = new Chronometre(l);
		
		timer.attach(chrono);
		timer.start();
		
        Scene scene = new Scene(new StackPane(l), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch();
    }

}
